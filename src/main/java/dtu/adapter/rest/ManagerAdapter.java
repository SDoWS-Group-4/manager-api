package dtu.adapter.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dtu.ManagerService;
import dtu.Payment;
import dtu.PaymentService;


public class ManagerAdapter {
    ManagerService service = new ManagerServiceFactory().getService();
    PaymentService paymentService = new PaymentServiceFactory().getService();

    public HashMap<String, ArrayList<Payment>> getReport() throws Exception {
        return paymentService.getReport();
    }
    }

