package dtu.adapter.rest;

import javax.ws.rs.core.Response;

import dtu.Account;
import dtu.BankService;

public class BankAdapter {
    BankService bankService = new BankServiceFactory().getService();

    public String register(Account account, String balance) throws Exception {
        if (account == null){throw new IllegalArgumentException("Account information cannot be null");}
        if (balance == null){throw new IllegalArgumentException("Balance cannot be null");}
        return bankService.register(account, balance);
    }

    public String deregister(String id) throws Exception {
        if (id == null){throw new IllegalArgumentException("id cannot be null");}
        return "The bank account with ID: " + bankService.deregister(id) + " was deregistered";
    }
}
