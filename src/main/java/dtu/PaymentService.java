package dtu;

import messaging.Event;
import messaging.MessageQueue;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class PaymentService {

    private MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<HashMap<String, ArrayList<Payment>>>> correlations = new ConcurrentHashMap<>();

    public PaymentService(MessageQueue q) {
        queue = q;
        queue.addHandler("AllPaymentsFound", this::handleAllPaymentsFound);
        queue.addHandler("AllPaymentsNotFound", this::handleAllPaymentsNotFound);
    }

    public HashMap<String, ArrayList<Payment>> getReport() throws Exception {
        try {
            var correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("AllPaymentsRequested", new Object[] {correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getCause() != null) {
                if (e.getCause().getClass() == NoSuchElementException.class) {
                    throw new IllegalArgumentException(e.getCause().getMessage());
                }
            }
            throw new Exception(e.getMessage());
        }
    }

    private void handleAllPaymentsFound(Event ev) {
        HashMap<String, ArrayList<Payment>> report = ev.getArgument(0, HashMap.class);
        var cid = ev.getArgument(1, CorrelationId.class);

        correlations.get(cid).complete(report);
    }

    private void handleAllPaymentsNotFound(Event ev) {
        NoSuchElementException exception = ev.getArgument(0, NoSuchElementException.class);
        var cid = ev.getArgument(1, CorrelationId.class);
        // correlations.get(cid).complete(exception);
        correlations.get(cid).completeExceptionally(exception);
    }
}
