package dtu;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import messaging.Event;
import messaging.MessageQueue;

public class BankService {

    private MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

    public BankService(MessageQueue q) {
        queue = q;
        queue.addHandler("AccountRegistered", this::handleAccountRegistered);
        queue.addHandler("AccountNotRegistered", this::handleAccountNotRegistered);
        queue.addHandler("AccountRetired", this::handleAccountRetired);
        queue.addHandler("AccountNotRetired", this::handleAccountNotRetired);
    }

    public String register(Account account, String balance) throws Exception {
        try {
            var correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("RegisterAccountRequested",
                    new Object[] { account, new BigDecimal(balance), correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (Exception e) {
            if (e.getCause() != null) {
                if (e.getCause().getClass() == Exception.class) {
                    throw new Exception(e.getCause().getMessage());
                }
            }
            throw new Exception(e.getMessage());
        }
    }

    public String deregister(String id) throws Exception{
        try {
            var correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());
            Event event = new Event("RetireAccountRequested",
                    new Object[] { id, correlationId });
            queue.publish(event);
            return correlations.get(correlationId).join();
        } catch (Exception e) {
            if (e.getCause() != null) {
                if (e.getCause().getClass() == Exception.class) {
                    throw new Exception(e.getCause().getMessage());
                }
            }
            throw new Exception(e.getMessage());
        }
    }

    private void handleAccountRegistered(Event e) {
        var accountId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        correlations.get(correlationId).complete(accountId);
    }

    private void handleAccountNotRegistered(Event e) {
        var exception = e.getArgument(0, Exception.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        correlations.get(correlationId).completeExceptionally(exception);
    }

    private void handleAccountRetired(Event e) {
        var accountId = e.getArgument(0, String.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        correlations.get(correlationId).complete(accountId);
    }

    private void handleAccountNotRetired(Event e) {
        var exception = e.getArgument(0, Exception.class);
        var correlationId = e.getArgument(1, CorrelationId.class);

        correlations.get(correlationId).completeExceptionally(exception);
    }

}
