package dtu.Exceptions;

public class MyInternalServerErrorException extends Exception {

    private static final long serialVersionUID = 1L;

    public MyInternalServerErrorException() {
    }

    public MyInternalServerErrorException(String message) {
        super(message);
    }

    public MyInternalServerErrorException(String message, Exception cause) {
        super(message, cause);
    }
}
