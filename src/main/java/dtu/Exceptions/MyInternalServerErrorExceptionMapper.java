package dtu.Exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class MyInternalServerErrorExceptionMapper implements ExceptionMapper<MyInternalServerErrorException> {

        @Override
        public Response toResponse(MyInternalServerErrorException exception) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(exception.getMessage()).build();
        }
}
