package dtu;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import messaging.Event;
import messaging.MessageQueue;

public class ManagerService {

	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<Customer>> correlations = new ConcurrentHashMap<>();

	public ManagerService(MessageQueue q) {
		queue = q;
		// queue.addHandler("CustomerIdAssigned", this::handleCustomerIdAssigned);
		// queue.addHandler("CustomerNotAssigned", this::handleCustomerNotAssigned);
	}

	// public Customer register(Customer m) throws IllegalArgumentException {
	// 	try {
	// 		var correlationId = CorrelationId.randomId();
	// 		correlations.put(correlationId, new CompletableFuture<>());
	// 		Event event = new Event("CustomerRegistrationRequested", new Object[] { m, correlationId });
	// 		queue.publish(event);

	// 		return correlations.get(correlationId).join();
	// 	} catch (IllegalArgumentException e) {
	// 		e.printStackTrace();
	// 		throw e; 
	// 	}
	// }

	// public void handleCustomerIdAssigned(Event e) {
	// 	var customer = e.getArgument(0, Customer.class);
	// 	var cid = e.getArgument(1, CorrelationId.class);

	// 	correlations.get(cid).complete(customer);
	// }

	// public void handleCustomerNotAssigned(Event e) {
	// 	var exception = e.getArgument(0, IllegalArgumentException.class);
	// 	var cid = e.getArgument(1, CorrelationId.class);

	// 	// correlations.get(cid).complete(exception);
	// 	correlations.get(cid).completeExceptionally(exception);
	// }
}
