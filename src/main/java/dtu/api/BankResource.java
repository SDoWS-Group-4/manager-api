package dtu.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import dtu.Account;
import dtu.Exceptions.MyBadRequestException;

import dtu.adapter.rest.BankAdapter;

@Path("/bank")
public class BankResource {

    BankAdapter bankAdapter = new BankAdapter();
    
    @Path("/register")
    @POST
    public String registerBankAccount(Account account, @QueryParam("balance") String balance) throws MyBadRequestException {

        try {
            return bankAdapter.register(account, balance);
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch (Exception ex) {
            throw new MyBadRequestException(ex.getMessage());
        }
    }


    @Path("/deregister")
    @DELETE
    public String deregisterBankAccount(@QueryParam("id") String id) throws MyBadRequestException {

        try {
            return bankAdapter.deregister(id);
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch (Exception ex) {
            throw new MyBadRequestException(ex.getMessage());
        }
    }
}
