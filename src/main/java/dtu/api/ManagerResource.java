package dtu.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dtu.Customer;
import dtu.Exceptions.MyNotFoundException;
import dtu.ManagerService;
import dtu.Payment;
import dtu.Exceptions.MyBadRequestException;
import dtu.adapter.rest.ManagerAdapter;

@Path("/manager")
public class ManagerResource {

    ManagerAdapter managerAdapter = new ManagerAdapter();

    @Path("/report")
    @GET
    @Produces("application/json")
    public HashMap<String, ArrayList<Payment>> getReportAPI() throws MyBadRequestException, MyNotFoundException {
        try {
            return managerAdapter.getReport();
        } catch (NoSuchElementException ex) {
            throw new MyNotFoundException(ex.getMessage());
        } catch (Exception ex1) {
            throw new MyBadRequestException(ex1.getMessage());
        }
    }
}
