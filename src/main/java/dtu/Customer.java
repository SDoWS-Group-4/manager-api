package dtu;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.Data;
@Data
public class Customer implements Serializable {

    @Getter @Setter private String id;
    @Getter @Setter private Double balance;
    @Getter @Setter private String name;
    @Getter @Setter private String bankAccount;

    public Customer() {
    }

    public Customer(String id, Double balance, String name, String bankAccount) {
        this.id = id;
        this.balance = balance;
        this.name = name;
        this.bankAccount = bankAccount;
    }
}
