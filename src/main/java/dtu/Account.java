package dtu;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Account implements Serializable {
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private String cprNumber;
    @Getter @Setter private String bankAccountID;

    public Account() {
    }
}
